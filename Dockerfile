FROM viniciusmmm/node-dockerized
WORKDIR  /app
CMD dockerize -wait tcp://postgres:5432 -timeout 360s yarn install && yarn start