# Introducao

Prototipo de uma api com forte tipagem. Entidades sao definidas em [typeorm](https://typeorm.io/#/), resolvers sao definidos em [type-graphql](https://typegraphql.ml/) e esquema e servido com o [apollo-server](https://www.apollographql.com/docs/apollo-server/)

# Iniciando aplicacao

```zsh
$ docker-compose build
$ docker-compose up
```

# Sync

Sync e o processo de realizar as migracoes do banco. Sempre que uma entidade em `src/entities` for modificada, o comando sync detecta essas modificacoes e as aplica no banco.

O banco nao vem com migracoes ja realizadas, logo depois de iniciar a primeira vez deve-se realizar sync.

```zsh
# Com containers rodando:
$ docker-compose run app bash
# Dentro do container:
$ cd /app
$ yarn schema:sync
```
