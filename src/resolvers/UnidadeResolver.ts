import { Resolver, Query, Mutation, Arg } from "type-graphql";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Unidade } from "../entity/Unidade";
import { Repository } from "typeorm";
import { UnidadeInput } from "./types/UnidadeInput";

@Resolver(Unidade)
export class UnidadeResolver {
  constructor(
    @InjectRepository(Unidade) private readonly unidadeRepo: Repository<Unidade>
  ) {}

  @Query(returns => [Unidade])
  unidades(args): Promise<Unidade[]> {
    return this.unidadeRepo.find();
  }

  @Mutation(returns => Unidade)
  createUnidade(@Arg("unidade") unidadeInput: UnidadeInput) {
    let unidade = this.unidadeRepo.create(unidadeInput);
    return this.unidadeRepo.save(unidade);
  }
}
