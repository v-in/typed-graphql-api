import { InputType, Field } from "type-graphql";
import { Unidade } from "../../entity/Unidade";

@InputType()
export class UnidadeInput implements Partial<Unidade> {
  @Field()
  codigo: number;

  @Field()
  sigla: string;

  @Field()
  hierarquia: string;

  @Field({ nullable: true })
  unidadeGestora: number;

  @Field()
  nome: string;
}
