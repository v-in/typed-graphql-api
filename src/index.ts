import "reflect-metadata";
import { createConnection, useContainer } from "typeorm";
import { buildSchema } from "type-graphql";
import { ApolloServer } from "apollo-server";
import { Container } from "typedi";
import { UnidadeResolver } from "./resolvers/UnidadeResolver";

useContainer(Container);

const startServer = async () => {
  try {
    const schema = await buildSchema({
      resolvers: [UnidadeResolver],
      container: Container
    });
    const conection = await createConnection();
    const server = new ApolloServer({ schema });
    const { url } = await server.listen(4000);
    console.log(`Server is running, GraphQL Playground available at ${url}`);
  } catch (e) {
    console.log(e);
  }
};

startServer();
