import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn
} from "typeorm";
import { Categoria } from "./Categoria";
import { ObjectType, Field, ID } from "type-graphql";

@Entity()
@ObjectType({ description: "Pontos avaliados durante a apresentacao" })
export class Pergunta {
  @Field(type => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ nullable: false })
  conteudoPergunta: string;

  @Field(type => Categoria)
  @OneToOne(type => Categoria)
  @JoinColumn()
  categoria: Categoria;
}
