import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { ObjectType, Field, ID } from "type-graphql";

@Entity()
@ObjectType({ description: "Unidade da UFPB" })
export class Unidade {
  @Field(type => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  codigo: number;

  @Field()
  @Column()
  nome: string;

  @Field()
  @Column()
  sigla: string;

  @Field()
  @Column()
  hierarquia: string;

  @Field({ nullable: true })
  //TODO: Adicionar relacionamento
  @Column({ nullable: true })
  unidadeGestora: number;

  @Field({ nullable: true })
  @Column({ nullable: true })
  tipo: string;
}
